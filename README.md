<div align="center"><h1><ins>DevOps Demo Project:</ins></h1></div> 
<div align="center"><h2>Deploy Microservices Application into Linode-Hosted Kubernetes with Security Best Practices </h2></div>

<div align="center">![1](img/devOps.png)</div>
<br>


<ins>DevOps Tools used in this project:</ins><br>- Git<br>- GitLab<br>- Kubernetes<br>- Linode LKE<br>- Google Container Registry<br>- Microsoft Visual Studio Code <br>- Windows PowerShell

<ins>Operating systems:</ins><br>- Kubernetes Pods: Linux<br>- Windows 11

<ins>Database used in this project:</ins><br>- Redis

<ins>Project Purpose:</ins> Deploy a microservices application into a Linode-hosted Kubernetes cluster. 

<ins>Out of scope:</ins> Installation of the kubectl command line tool

# Project Walkthrough
I. [Preliminary Info and Assumptions](#preliminary)<br>
II. [Initial Setup](#initial)<br>
III. [K8S Manifest Configurations](#manifests)<br>
IV. [Create K8s Cluster in Linode & K8s Deployment](#linode)<br>
V. [Troubleshooting Existing K8s Deployment](#troubleshoot)<br>
VI. [Verify Microservices Application Functionality](#verify)<br>
VII. [Security Best Practices & Additional Troubleshooting](#security)<br>
VIII. [Project Teardown](#teardown)

## I. Preliminary Info and Assumptions <a name="preliminary"></a>
1. The source code is an official online shop project from Google, forked by GitHub user nanuchi, and is located here: https://github.com/nanuchi/microservices-demo/tree/master/src 

    As part of this project, the microservices that will be deployed for the online shop application are listed below:<br>- adservice<br>- cartservice<br>- checkoutservice<br>- currencyservice<br>- emailservice<br>- frontend<br>- paymentservice<br>- productcatalogservice<br>- recommendationservice<br>- shippingservice

    <ins>Note:</ins> the loadgenerator microservice is optional, will not be deployed as part of this project, and is solely used for testing application load.


2. In advance, assume the developers tell the DevOps team how each microservice communicates with each other. A visual representation is shown below:
![1p](img/preliminary/1.png)
<br>**&copy; Google and TechWorld with Nana DevOps Bootcamp**
 
3. The only third-party service or database that is required is <b>Redis</b>.<br>- Redis is used to store data in the shopping cart (i.e. the cartservice microservice).

4. The <b>Frontend</b> service is externally accessible outside the cluster and will be getting all the requests from the browser.
5. The image names for each microservice all have Google Container Registry URLs. A full list can be found here: https://gcr.io/google-samples/microservices-demo/  
6. Assume the developers tell the DevOps team the following info in advance:<br>- Which environment variables each microservice expects<br>- On which port each microservice starts

7. For this project, assume there is a single development team and all microservices will use the same namespace.

## II. Initial Setup <a name="initial"></a>
1.	Open PowerShell and create a folder named “online-shop-microservices” at the root of C:\ 

        cd ..
        mkdir online-shop-microservices
 
2.	Open Microsoft Visual Studio Code. In the top left-hand corner, navigate to the Hamburger Menu (≡) > File > Open Folder. Browse to C:\open-shop-microservices and click Select Folder. If prompted, click Trust this project. Create a new file called config.yaml.

    ![1is](img/initialsetup/1.png)
 
3. The starting code below acts as a minimal configuration template for the deployment and service of  one microservice. The xxx denotes values that need to be changed for each microservice. Copy and paste the below code into config.yaml 11 times:
        
        ---
        apiVersion: apps/v1
        kind: Deployment
        metadata:
          name: xxx
        spec:
          selector:
            matchLabels:
              app: xxx
          template:
            metadata:
              labels:
                app: xxx
            spec:
              containers:
              - name: xxx
                image: xxx
                ports:
                - containerPort: xxx
        ---
        apiVersion: v1
        kind: Service
        metadata:
          name: xxx
        spec:
          type: ClusterIP
          selector:
            app: xxx
          ports:
          - protocol: TCP
            port: xxx
            targetPort: xxx

4.	In Microsoft Visual Studio Code, verify the template has been copied 11 times by using the Find feature (Ctrl+F). Type in <i>kind: Deployment</i> and confirm 11 entries exist.
 ![2is](img/initialsetup/2.png)

5.	The initial project setup is complete. The Kubernetes manifest configuration can now be written.

## III. K8s Manifest Configurations <a name="manifests"></a>

1.	For the first deployment and service instance in config.yaml, set the microservice name (i.e. emailservice) as the value for:<br>- The deployment name and service name under metadata > name<br>- The pod name under spec > template > metadata > labels > app

    Group the pod, deployment, and services under the same name by specifying emailservice as the value for:<br>- spec > selector > matchLabels > app <br>- spec > selector > app

    The following screenshot illustrates these changes:

      ![1m](img/manifests/1.png)

2.	In deployment, the container name (under spec > template > spec > containers > name) will be set as <i>service</i> for all 11 microservices. The image values for all microservices will be set as the corresponding Google Container Registry URL located here: https://gcr.io/google-samples/microservices-demo/   
3.	Assume that port details will be supplied by the developers in advance. For the emailservice microservice, specify the containerPort (where the application inside the container will start) and the targetPort (where the service forwards requests to the application inside the pod) as 8080. 
4.	For the emailservice microservice, set the service port (under spec > ports > port) as 5000. 
5.	Assume the developers tell the DevOps team in advance the emailservice microservice expects an environment variable called PORT. The environment variable and associated value of 8080 are added to the containers specification. The below screenshot summarizes steps 2-5. Configuration for the emailservice microservice is now complete.

    ![2m](img/manifests/2.png)

6.	Repeat steps 1-5 for the remaining microservices, with important additions, changes, and notes below:
    -	<ins>recommendationservice</ins>:<br>- Service port (port) is 8080<br>-	Add an additional environment variable PRODUCT_CATALOG_SERVICE_ADDR with value “productcatalogservice:3550”
    -	<ins>productcatalogservice</ins>: Container port, target port, and service port (port) are all 3550
    -	<ins>paymentservice and shippingservice</ins>: Container port, target port, and service port (port) are all 50051
    -	<ins>currencyservice</ins>: container port, target port, and service port (port) are all 7000
    -	<ins>adservice</ins>: container port, target port, and service port (port) are all 9555
    -	<ins>cartservice</ins>:<br>- container port, target port, and service port (port) are all 7070<br>- Add an additional environmental variable REDIS_ADDR with value “redis-cart:6379”
    - <ins>redis-cart</ins>:<br>-	Deployment name, service name, and selector labels are all <i>redis-cart</i><br>-	Container name is <i>redis</i><br>- Image is redis:alpine<br>-	Container port, target port, and service port (port) are all 6379 <br>-	No environmental variables set <br>-	Set emptyDir volume for the redis application to persist data temporarily in memory <br>-	Define emptyDir volume under the pod specification 

          volumes:
          - name: redis-data
            emptyDir: {}
                
      -Mount the emptyDir volume into the container under the container specification.

          volumeMounts:
          - name: redis-data
            mountPath: /data

    - <ins>checkoutservice</ins>: <br>-	Container port, target port, and service port (port) are all 5050<br>-	Additional environment variables needed to communicate with 6 microservices are:

          - name: PRODUCT_CATALOG_SERVICE_ADDR
            value: "productcatalogservice:3550"
          - name: SHIPPING_SERVICE_ADDR
            value: "shippingservice:50051"
          - name: PAYMENT_SERVICE_ADDR
            value: "paymentservice:50051"
          - name: EMAIL_SERVICE_ADDR
            value: "emailservice:5000"
          - name: CURRENCY_SERVICE_ADDR
            value: "currencyservice:7000"
          - name: CART_SERVICE_ADDR
            value: "cartservice:7070"
    - <ins>frontend</ins>:<br>- Container port and target port are both 8080<br>- Service port (port) is 80<br>- Additional environment variables needed to communicate with 7 microservices are:

          - name: PRODUCT_CATALOG_SERVICE_ADDR
            value: "productcatalogservice:3550"
          - name: CURRENCY_SERVICE_ADDR
            value: "currencyservice:7000" 
          - name: CART_SERVICE_ADDR
            value: "cartservice:7070"
          - name: RECOMMENDATION_SERVICE_ADDR
            value: "recommendationservice:8080"
          - name: SHIPPING_SERVICE_ADDR
            value: "shippingservice:50051"
          - name: CHECKOUT_SERVICE_ADDR
            value: "checkoutservice:5050"
          - name: AD_SERVICE_ADDR
            value: "adservice:9555"
      -The frontend must communicate externally, so in service, spec > type must be set as NodePort with a third port attribute defined as shown below:

          ports:
          - protocol: TCP 
            port: 80
            targetPort: 8080
            nodePort: 30007
 
All microservices are now configured inside the Kubernetes configuration file.

## IV. Create K8s Cluster in Linode & K8s Deployment <a name="linode"></a>

1.	In a browser, navigate to https://www.linode.com and sign in with credentials.
2.	Click the Hamburger icon (≡) > Kubernetes > Create Cluster. <br>
![1l](img/linode/1.png)
 
3.	Specify the Cluster Label as online-shop-microservices. Specify the region as the one closest to the end users' geographic location and select the latest Kubernetes version available (1.26 as of this writing). For the purpose of this project, select the No radio button under HA Control Plane.
![2l](img/linode/2.png)

4.	Under the Add Node Pools section, select the Shared CPU tab. Select the Linode 2GB option with a quantity of 3.
![3l](img/linode/3.png)
 
5.	Click the <b>Create Cluster</b> button.

    ![4l](img/linode/4.png)
 
6.	Wait 10-15 minutes for the cluster to change from the Provisioning to Ready status. 
7.	Once the cluster is in the Ready status, download the kubeconfig file and save it to the Downloads folder. This file will be needed to connect to the newly created K8s cluster.

    ![5](img/linode/5.png)
 
8.	Windows 11: Since the kubeconfig file contains credentials to the Linode K8s cluster, its permissions must be restricted. Navigate to the Downloads folder, right-click on the online-shop-microservices-kubeconfig.yaml file and select Properties. On the Security tab, click Advanced. Disable inheritance and convert to explicit permissions. Set Read-Only permissions for the Windows user name and remove all other permissions (including System and other groups). 
9.	Open PowerShell and export the kubeconfig file to the KUBECONFIG environmental variable. 

        $env:KUBECONFIG="C:\Users\username\Downloads\online-shop-microservices-kubeconfig.yaml"

10.	<ins>Note:</ins> This project walkthrough assumes the kubectl command line tool is already installed on Windows 11. Instructions from the official Kubernetes site can be found here: https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/

    In PowerShell, test the connection to the Linode cluster by entering: 

        kubectl get node

    The following output displays:

      ![6l](img/linode/6.png)
 
11.	In PowerShell, change to the project folder (C:\online-shop-microservices). 

        cd ../..
        cd online-shop-microservices

    Create a new namespace called microservices.

        kubectl create ns microservices

    Output will display indicating the microservices namespace is created.

    ![7l](img/linode/7.png)
 
12.	Deploy the microservices to the Linode-hosted K8s cluster in the microservices namespace by entering:

        kubectl apply -f config.yaml -n microservices

    Output will display indicating the deployment and service for each microservice is created.

    ![8l](img/linode/8.png)

13.	Verify the pod status by running: 

        kubectl get pod -n microservices

    ![9l](img/linode/9.png)
 
## V. Troubleshooting Existing K8s Deployment <a name="troubleshoot"></a>

1.	From the PowerShell output in Step 13 of Part IV, notice the currencyservice and redis-cart microservices are in the running state. All other microservices have an ErrImagePull or ImagePullBackOff status. Start troubleshooting by appending a version tag to the end of each image URL in config.yaml. The version tag can be found by navigating to the Google Container Registry (https://gcr.io/google-samples/microservices-demo/), clicking on the respective microservice folder, and locating the latest version tag. For example, here is a screenshot of the latest tag version for adservice:<br>

    ![1t](img/troubleshoot/1.png)
 
2.	Restore the project folder within Microsoft Visual Studio code. For each microservice in an ErrImagePull or ImagePullBackOff state, append the version tag to the end of the image URL by entering “:v0.8.0.” For example, the adservice image will now look like this:

        image: gcr.io/google-samples/microservices-demo/adservice:v0.8.0
        
3.	Save the config.yaml file, and rerun the following command in PowerShell:

        kubectl apply -f config.yaml -n microservices
4.	Verify pod status by rerunning the following command: 

        kubectl get pod -n microservices 

    <ins>Note</ins>: it may take some time for the existing pods to terminate.

5.	After about a minute, the output from running the previous command now looks like this:
![2t](img/troubleshoot/2.png)
 
6.	Notice all microservices are now in the running status, except for paymentservice. Debug this failing microservice by entering:

        kubectl logs <pod name> -n microservices

    The following output is displayed, which mentions a project ID needing to be specified in the configuration.
![3t](img/troubleshoot/3.png)
 
7.	To resolve this issue, refer to this link: https://github.com/GoogleCloudPlatform/microservices-demo/issues/647

    In the config.yaml file, locate the payment service configuration and specify the following two additional environment variables. Set their values to 1:

        - name: DISABLE_PROFILER
          value: "1"
        - name: DISABLE_DEBUGGER  
          value: "1"

8.	Save the config.yaml file and rerun the following command:

        kubectl apply -f config.yaml -n microservices

9.	Wait for the old paymentservice pod to terminate and recreate. Reenter the command:

        kubectl get pod -n microservices

    The output shows all microservices are now running:

    ![4t](img/troubleshoot/4.png)
  
## VI. Verify Microservices Application Functionality <a name="verify"></a>

1.	Now that the pods are all in a running state, view the list of services created by entering the following command:

        kubectl get svc -n microservices

    The output will display the list of service components for each microservice.

    ![1v](img/verify/1.png)

    Notice all services have a type of ClusterIP, which means they are private IP addresses. The only exception is the frontend, which has the type NodePort, as it will be accessible externally from a browser. The NodePort type opens port 30007 on the 3 worker nodes.

2.	Test the microservices application functionality by accessing it through a browser. This can be achieved by navigating back to the Linode Kubernetes portal and viewing the public IP of one of the worker nodes. In a new browser tab, enter the public IP address of one of the worker nodes and specify port 30007.

    ![2v](img/verify/2.png)
 
3.	Perform additional testing by clicking on the different product links, the cart icon, and simulating a purchase.

## VII. Security Best Practices & Additional Troubleshooting <a name="security"></a>

The creation of the Kubernetes manifests up to this point serves the function of getting the microservices application up and running inside the Kubernetes cluster. However, optimization of the config.yaml file needs to occur in order to observe security best practices.

Please reference the config-best-practices.yaml file moving forward. A walkthrough of security best practices and their implementation are provided below:
 
1.	<b>Specify tag version for all container images:</b> Recall in Section V of this project walkthrough that tags are added to get the pods in a running state. Accordingly, most of this best practice is already implemented. However, the tag version still needs to be specified for the currencyservice pod that was running successfully prior to troubleshooting. Update the currencyservice image value as follows:

        image: gcr.io/google-samples/microservices-demo/currencyservice:v0.8.0

    Save the configuration and rerun the following command:

        kubectl apply -f config.yaml -n microservices

    Notice the currencyservice pod is an error state. Rerun the following command to determine the issue:

        kubectl logs <pod name> -n microservices

    Once again, the Project ID issue resurfaces but this time for the currencyservice pod. 

    ![1s](img/security/1.png)

    Add two additional environment variables as outlined in Part V, step 7 of the troubleshooting section.

        - name: DISABLE_PROFILER
          value: "1"
        - name: DISABLE_DEBUGGER
          value: "1"

    Save the config, and reissue the following commands:

        kubectl apply -f config.yaml -n microservices
        kubectl get pod -n microservices

    The currencyservice pod is once again functional.

    ![2s](img/security/2.png)
 
    <ins>Take-away:</ins> One of the main reasons specifying the version tag in the image attribute is a best practice is because of what was just demonstrated here and in Part V with pods crashing. In addition, forgoing the use of version tags will always pull the latest tag of the image. This is not a desired outcome if the version has not been tested! 

2.	<b>Liveness Probe for every container:</b>  While K8s has self-healing abilities with restarting crashing pods, pod health is independent of container/application health. In other words, the pod can be reporting healthy even if the container/application itself is in a crash loop. The livenessProbe attribute bridges the gap for monitoring the container/application’s own health and can notify Kubernetes to restart a pod when the application is in an unhealthy state. 

    Two important parameters for the livenessProbe attribute are:<br>- <b>periodSeconds</b>: specifies the frequency (every 5 seconds) in which the application health check is performed; pings the application every 5 seconds to check the response of the application. <br>- <b>exec: command</b>: specifies the path of the health check script/program and verifies if the application is running on the given port. 

    For the sake of this project, assume the developers added the script/command to each microservice image. 

    In the deployment configuration, the following code is added under spec > containers:

        periodSeconds: 5
        exec:
          command: ["/bin/grpc_health_probe", "-addr=:8080"}

    Except for redis-cart (for now), copy and paste this code under the container specification for each microservice. Update the -addr=: attribute to correspond to the respective port.

    Save the configuration and issue the following command to apply the changes. 

        kubectl apply -f config-best-practices.yaml -n microservices

    View the pod status

        kubectl get pod -n microservices

    Wait a moment for the pods to terminate. All pods should be running:

    ![3s](img/security/3.png)

3.	<b>Readiness Probe for every container:</b> The readinessProbe attribute assesses application/container health and availability at startup and tells Kubernetes the application is prepared for traffic to be received. 

    The configuration is identical to that of the livenessProbe attribute. Just as before, the deployment configuration requires the following code under spec > containers:

        periodSeconds: 5
        exec:
          command: ["/bin/grpc_health_probe", "-addr=:8080"]

    The periodSeconds parameter, in this case, can be helpful if the application requires a certain amount of time to start; this allows a delay to occur which may reduce the number of startup-related errors in the logs.

    While livenessProbe and readinessProbe attributes are both health checks, the attributes assess health at different times. The readinessProbe attribute checks health at startup and then health checks are handed off to the livenessProbe when the application is in a running state.

    Except for redis-cart (for now), the above code is copied and pasted into the deployment configuration of every microservice under spec > containers. Be sure to adjust the port number to the respective port of the microservice.

    <ins>Troubleshooting and resolution:</ins> The latest image tag (v0.8.0) cannot be used for this project (except for frontend), as a common error in kubectl logs relating to Google application credentials and StackDriver Exporter prevents the pods from starting. The issues are documented here: https://github.com/GoogleCloudPlatform/microservices-demo/issues/316. 

    The following changes to config-best-practices.yaml are made to get all pods back in the running and ready states:
    - <ins>Emailservice and recommendationservice</ins>:<br>- Image version back-revved to v0.2.3<br>- Additional environment variable DISABLE_TRACING set to a value of “1”
    - Productcatalogservice, shippingservice, cartservice, and checkoutservice image versions back-revved to v0.2.3
    - <ins>paymentservice and currencyservice</ins>:<br>- Image version back-revved to v0.2.3<br>- Additional environment variables DISABLE_PROFILER and DISABLE_DEBUGGER set to a value of “1”
    - <ins>adservice</ins>:<br>- Image version back-revved to 0.3.4 (v0.2.3 no longer exists in the Google repository)<br>- Additional environmental variable DISABLE_PROFILER set to value of “1”
    - <ins>frontend</ins>:<br>- With the readinessProbe attribute enabled, any previous back-revved image version did not get the pod in a running state.<br>- Additional environment variables DISABLE_PROFILER, DISABLE_TRACING, and DISABLE_DEBUGGING were set to a value of “1” for troubleshooting, but the frontend pod is still not in a ready state:
        
        ![4s](img/security/4.png)
      <br>- As such, the readinessProbe code can be commented out for now and will be addressed with the alternative health check method (HTTP probe covered later).
      <br>- The image version is kept at v0.8.0 to update the UI.

    With the above changes to config-best-practices.yaml, all microservices are again in a ready and running state:

    ![5s](img/security/5.png)
 
    Aside from command execution, two other methods exist for checking the health status in the readinessProbe and livenessProbe attributes: (1) TCP probe and (2) HTTP probe. For this project, TCP Probe is used to check the health of the redis-cart application and the HTTP probe is used to check the health of the frontend microservice.

    A)	With TCP probe, application health is based on whether a probe connection on the specified port at the node level is successful or not. If kubectl successfully opens a socket to the container on the specified port, the application is reported to be healthy. Otherwise, the application is in an unhealthy state.

    The TCP probe method is used to check the health of the redis-cart microservice. The following code is added to the readinessProbe and livenessProbe attributes. In this case, there is a delay of 5 seconds before the readinessProbe and livenessProbe are run:

        initialDelaySeconds: 5
        periodSeconds: 5
        tcpSocket: 
          port: 6379

    This waiting behavior is especially helpful if a particular application requires a delayed start prior to being fully ready.  
 
    B)	The HTTP probe uses the httpGet attribute, whereby application health is based on its response to an HTTP request. A path and port are provided as parameters. The path parameter specifies the location of the health check URL, and the port is the same as the container port.

    The HTTP probe method is used to check the health of the frontend microservice. The following code is added to both readinessProbe and livenessProbe attributes. In this case, there is a delay of 10 seconds before the first readinessProbe and livenessProbe are run:

        initialDelaySeconds: 10
        httpGet:
          path: "/_healthz"
          port: 8080

    With the addition of the TCP probe and HTTP probe health checks, the code is saved, and the following commands are rerun:

        kubectl apply -f config-best-practices.yaml -n microservices
        kubectl get pod -n microservices

    The microservices are still ready and running.

      ![6s](img/security/6.png)

4. <b> Resource Requests for each container:</b>  Resource requests allocate a specified amount of CPU/RAM resources for each container anticipated during normal operation. The developers of these microservices should be consulted in determining these values as they are likely to know any nuances and the finer details. 

    The Kubernetes Scheduler utilizes resource requests to determine where the pod will reside. 

    Under the container definition in config-best-practices.yaml, the following code is copied and pasted under each microservice.  CPU values are expressed in millicores (1,000 millicores = 1 core), while RAM values are expressed in mebibytes (Mi). 

        requests:
          cpu: 100m
          memory: 64Mi  

    The general best practice of allocating 1 CPU or less is observed. Notable exceptions include adservice and redis-cart, which require more RAM. Redis-cart requires even less than 100m CPU. 

    The configuration is then saved, and the following command is issued to apply changes:  

        kubectl apply -f config-best-practices.yaml -n microservices

    The pod status is reviewed with the following command. This command is rerun until previous pods are terminated and new ones are generated:

        kubectl get pod -n microservices

    All pods are still running:

    ![7s](img/security/7.png)
 
5.	<b>Resource Limits for each container:</b>  Resource limits address the need for additional resource allocation during heavy application load. The additional resource allocation is also helpful in avoiding scenarios of runaway processes (e.g. infinite loop bugs) and/or one pod exhausting the remaining resources on the node. Using resource limits to prevent containers from exceeding a specified resource threshold allows containers to continue to get the resources they need to run.

    In the config-best-practices.yaml file, resource limits for CPU/RAM are doubled for every microservice. A pod will never be scheduled if the specified value is greater than the largest node.

    Under the containers: resources definition in config-best-practices.yaml, the following code is copied and pasted under each microservice.

        limits:
          cpu: 200m
          memory: 128Mi  

    Exceptions include the more CPU/RAM-intensive adservice, with 300m CPU and 300Mi memory, and redis-cart, with 125m CPU and 300Mi memory.

    The configuration is then saved, and the following command is issued to apply changes:  


        kubectl apply -f config-best-practices.yaml -n microservices

    The pod status is reviewed with the following command. This command is rerun until previous pods are terminated and new ones are generated:

        kubectl get pod -n microservices

    <ins>Troubleshooting:</ins> It is observed that the adservice microservice is in a crash loop.

      ![8s](img/security/8.png)
 

    <ins>Resolution:</ins> Under both livenessProbe and readinessProbe attributes, setting the initialDelaySeconds parameter to 20 and the periodSeconds parameter to 15 successfully resolved the crash loop. The following command is reissued, and all pods are ready and running.

        kubectl get pod -n microservices

      ![9s](img/security/9.png)

6.	<b>Use LoadBalancer (or Ingress) Instead of NodePort:</b> The NodePort service type should not be used in Kubernetes manifests due to the security risks of external and direct port exposure on all worker nodes of the cluster. Using LoadBalancer as the frontend microservice’s service type remedies this security issue by limiting the attack surface to one point of entry (i.e. Linode’s load balancer). Thereafter, the load balancer will relay cluster requests to all other internal-facing microservices (services specified with the ClusterIP service type). 
 
    To implement the LoadBalancer service type, two changes are made to the frontend service configuration. First, the spec: type is changed to LoadBalancer and the nodePort line is deleted. 

    The configuration is then saved, and the following command is issued to apply changes:  

        kubectl apply -f config-best-practices.yaml -n microservices

    The pod status is reviewed with the following command. 

        kubectl get pod -n microservices

      ![10s](img/security/10.png)

    After logging into the Linode management portal and clicking Node Balancers as a menu option in the left pane, the load balancer is now visible with a single IP address.

      ![11s](img/security/11.png) 

7.	<b>Specify >1 Pod Replica and >1 Worker Node:</b><br> 

    <i>>1 Pod Replica:</i>
    
    If the spec: replicas attribute in the deployment is omitted, Kubernetes assumes the default replica value of 1. It is best practice to specify the spec: replicas attribute with a value of at least 2 for high availability and to eliminate a single point of failure. In the config-best-practices.yaml file, the following code is copied/pasted under specification (spec) for each microservice:

        replicas: 2

    The configuration is then saved, and the following command is issued to apply changes:  

        kubectl apply -f config-best-practices.yaml -n microservices

    The pod status is reviewed with the following command: 

        kubectl get pod -n microservices

    ![12s](img/security/12.png)

    <i>>1 Worker Node:</i>

    An ideal time to specify the number of worker nodes in the Kubernetes cluster is when the cluster is initially created. This best practice is adhered to in Part IV, step 4 of this project walkthrough. The same principle of eliminating a single failure point applies, and therefore, at least 2 worker nodes should be implemented in the cluster regardless of size or complexity. 

    If the nodeSelector attribute is used in the pod specification, replicas can then exist on different worker nodes, and in turn, facilitate server maintenance tasks.

8.	<b>Label everything:</b> Throughout this project, labels are applied to group pods, deployments, and services together.

9.	<b>Namespaces:</b> As opposed to the default namespace, this project uses the ‘microservices’ namespace for resource isolation. Implementing namespaces facilitates cluster administration and improves security by limiting which team has which privilege to a given namespace. 

10.	<b>Perform vulnerability and security scans:</b> Although outside the scope of this project, the best practice of performing vulnerability and security scans applies to all images (especially third-party), tools, and libraries. These scans can be performed manually or as a stage in a CI/CD pipeline.

11.	<b>Avoid Running Containers with Root Access:</b> The root user possesses unfettered access, and if enabled to run a container, this may have severe consequences during a security breach. Most official Docker images do not implement root user capabilities, though non-official third-party images are more of a concern. It is for this reason the official Docker image is used for the redis-cart. 

12.	<b>Kubernetes Version Updates:</b> For this project and as of this writing, the latest Kubernetes version available on Linode is used when initially spinning up the cluster. Subsequent versions provide security patches to add further protection from vulnerabilities. Updates are performed at the node group level first, followed by the cluster level, and should be carefully orchestrated to minimize the chance of an outage. Recall implementing multiple replicas (Best Practice #7) will facilitate server maintenance tasks, such as performing version upgrades.

## VIII. Project Teardown <a name="teardown"></a>

With the project complete, the Linode-hosted Kubernetes resources will be deleted to prevent incuring additional costs.

1.	Login to the Linode management portal. Click on Kubernetes in the left-hand pane and click Delete.

    ![1td](img/teardown/1.png)

2.	Enter the cluster name and click the <b>Delete Cluster</b> button.

    ![2td](img/teardown/2.png)

3.	Click on Node Balancers in the left pane and click Delete.

    ![3td](img/teardown/3.png)


4.	Copy and paste the name of the load balancer and click the <b>Delete</b> button.

    ![4td](img/teardown/4.png)

All Kubernetes resources have been successfully removed and no additional charges in Linode will incur.
